package com.example.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView res;
        EditText edtWeight, edtHinf, edtHinI;
        AppCompatButton btn;

        btn = findViewById(R.id.btn);
        res = findViewById(R.id.res);
        edtWeight = findViewById(R.id.edtWeight);
        edtHinf = findViewById(R.id.edtHtinf);
        edtHinI = findViewById(R.id.edtHtinI);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int wt = Integer.parseInt(edtWeight.getText().toString());
                int ht = Integer.parseInt(edtHinf.getText().toString());
                int ht1 = Integer.parseInt(edtHinI.getText().toString());
                int totalIn = ht * 12 + ht1;

                double totalCm = totalIn * 2.53;
                double totalM = totalCm / 100;

                double bmi = wt / (totalM * totalM);
                if (bmi>25)
                {
                    res.setText("You are Over Weight");
                }
                else if(bmi<18){
                    res.setText("You are under weight");
                }
                else
                {
                    res.setText("You are Healthy");
                }
            }
        });
    }
}